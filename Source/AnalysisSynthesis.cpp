/*
  ==============================================================================

    AnalysisSynthesis.cpp
    Created: 9 Jun 2020 10:30:48am
    Author:  Julian Vanasse

  ==============================================================================
*/

#define PI 3.14159265358979
#include "AnalysisSynthesis.h"

AnalysisSynthesis::AnalysisSynthesis() : transformFFT(fftOrder)
{
    // init buffer dims
    numBufferChannels = ceil(static_cast<float>(windowSize)/static_cast<float>(hopSize));
    bufferSize = windowSize;
    numFrequencyBins = ceil(static_cast<float>(windowSize)/2.0f) + 1;
    
    // init buffers
    analysisBuffer = AudioBuffer<float>(numBufferChannels, bufferSize);
    synthesisBuffer = AudioBuffer<float>(numBufferChannels, bufferSize);
    
    // init window
    window = hann(windowSize);
    windowGain = 1;
    
    // init rw pos
    initRWPositions();

}

AnalysisSynthesis::~AnalysisSynthesis()
{
}

void AnalysisSynthesis::process(AudioBuffer<float> &buffer)
{
    int numInputChannels = buffer.getNumChannels();
    int numInputSamples = buffer.getNumSamples();
    
    AudioBuffer<float> inputBuffer(numInputChannels, numInputSamples);
    inputBuffer.makeCopyOf(buffer);
    
    buffer.clear();
    
    int inputChannelIdx = 0;
    for (int n = 0; n < numInputSamples; n++)
    {
        for (int b = 0; b < numBufferChannels; b++)
        {
            // write to b-th analysis channel
            analysisBuffer.setSample(b, analysisWritePositions[b], inputBuffer.getSample(inputChannelIdx, n));
            
            // if write pos is at end of buffer -> asynchronous processing
            if (analysisWritePositions[b] == bufferSize-1)
            {
                // copy to synthesisBuffer
                synthesisBuffer.copyFrom(b, 0, analysisBuffer, b, 0, bufferSize);
                
                // apply window
                applyWindow(synthesisBuffer, b);
                
                // process
                spectral(synthesisBuffer.getReadPointer(b), synthesisBuffer.getWritePointer(b));
                
                // post process
                applyWindow(synthesisBuffer, b);
            }
            
            // overlap and add
            buffer.setSample(inputChannelIdx, n, buffer.getSample(inputChannelIdx, n) + synthesisBuffer.getSample(b, synthesisReadPositions[b]));
        }
        
        // update rw
        updateRWPositions();
        
    }
}


void AnalysisSynthesis::spectral(const float *readPosition, float *writePosition)
{
    // clear
    for (int i = 0; i < fftSize*2; i++)
    {
        fftBuffer[i] = 0.0f;
    }
    
    // copy fftBuffer
    for (int i = 0; i < bufferSize; i++)
    {
        fftBuffer[i] = readPosition[i];
    }
    
    // transform
    transformFFT.performRealOnlyForwardTransform(fftBuffer, true);

    // update magnitude and phase
    cart2pol();
    
    // do something ...
    for (int l = 0; l < 30; l++)
    {
        magnitude[l] = 0.0f; // here we high pass in the time frequency domain :)
    }
    
    // update fftBuffer
    pol2cart();
    
    // inverse
    transformFFT.performRealOnlyInverseTransform(fftBuffer);
    
    // write to synthesisBuffer
    for (int i = 0; i < bufferSize; i++)
    {
        writePosition[i] = fftBuffer[i];
    }
    
}

void AnalysisSynthesis::cart2pol()
{
    int idx = 0;
    for (int k = 0; k < fftSize+1; k+=2)
    {
        // car2pol
        float r = mag(fftBuffer[k], fftBuffer[k+1]);
        float phi = ang(fftBuffer[k], fftBuffer[k+1]);
        
        // store
        magnitude[idx] = r;
        phase[idx] = phi;
        
        // inc
        idx ++;
    }
}

void AnalysisSynthesis::pol2cart()
{
    int idx = 0;
    for (int k = 0; k < fftSize+1; k+=2)
    {
        // pol2cart
        fftBuffer[k+1] = magnitude[idx] * cos(phase[idx]);
        fftBuffer[k] = magnitude[idx] * sin(phase[idx]);
        
        // inc
        idx++;
    }
}

float AnalysisSynthesis::mag(float a, float b)
{
    return sqrt(pow(a, 2.0f) + pow(b, 2.0f));
}

float AnalysisSynthesis::ang(float a, float b)
{
    return atan2(a, b);
}


void AnalysisSynthesis::initRWPositions()
{
    // clear arrays
    analysisWritePositions.clear();
    synthesisReadPositions.clear();
    
    // for each buffer channel, cascade rw pos
    for (int i = 0; i < numBufferChannels; i++)
    {
        int pos = i * hopSize;
        analysisWritePositions.add(pos);
    }
    
    // copy
    synthesisReadPositions.addArray(analysisWritePositions.getRawDataPointer(), numBufferChannels);
}

void AnalysisSynthesis::updateRWPositions()
{
    for (int k = 0; k < numBufferChannels; k++)
    {
        analysisWritePositions.set(k, (analysisWritePositions[k]+1) % bufferSize);
        synthesisReadPositions.set(k, (synthesisReadPositions[k]+1) % bufferSize);
    }
}

void AnalysisSynthesis::applyWindow(AudioBuffer<float> &buffer, int channel)
{
    for (int n = 0; n < windowSize; n++)
    {
        buffer.setSample(channel, n, buffer.getSample(channel, n) * window.getSample(0, n));
    }
}

AudioBuffer<float> AnalysisSynthesis::hann(int N)
{
    // empty buffer
    AudioBuffer<float> w = AudioBuffer<float>(1, N);
    w.clear();
    
    // iterate
    for (int n = 0; n < N; n++)
    {
        // assigne hann value h to each sample
        float h = pow(sin(static_cast<float>(PI * n)/static_cast<float>(N)), 2);
        w.setSample(0, n, h);
    }
    
    return w;
}
