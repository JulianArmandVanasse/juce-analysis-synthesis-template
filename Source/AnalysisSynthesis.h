/*
  ==============================================================================

    AnalysisSynthesis.h
    Created: 9 Jun 2020 10:30:48am
    Author:  Julian Vanasse

  ==============================================================================
*/

#pragma once
#include "../JuceLibraryCode/JuceHeader.h"

class AnalysisSynthesis
{
public:
    //***************** Constructor *****************
    AnalysisSynthesis();
    ~AnalysisSynthesis();
    
    //***************** Constants *******************
    static constexpr auto fftOrder = 10;
    static constexpr auto fftSize  = 1 << fftOrder;
    
    //***************** Methods *********************
    void process(AudioBuffer<float> &buffer);
    
    //***************** Variables *******************
    int hopSize {128};
    int windowSize {1024};
    
private:
    
    // window
    AudioBuffer<float> window;
    float windowGain;
    
    // buffer dims
    int numBufferChannels;
    int bufferSize;
    int numFrequencyBins;
    
    // buffers
    AudioBuffer<float> analysisBuffer;
    AudioBuffer<float> synthesisBuffer;
    
    // buffer rw positions
    Array<int> analysisWritePositions;
    Array<int> synthesisReadPositions;
    
    // fft
    dsp::FFT transformFFT;
    float fftBuffer[fftSize * 2];
    float magnitude[fftSize + 1];
    float phase[fftSize + 1];
    
    void cart2pol();
    void pol2cart();
    float mag(float, float);
    float ang(float, float);
    
    
    //***************** Methods *********************
    void spectral(float const *readPosition, float *writePosition);
    
    // rw positions
    void initRWPositions();
    void updateRWPositions();
    
    // window specific
    void applyWindow(AudioBuffer<float> &buffer, int channel);
    AudioBuffer<float> hann(int);
    
};
